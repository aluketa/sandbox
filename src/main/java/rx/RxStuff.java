package rx;

import rx.functions.Func1;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class RxStuff {
    private static ExecutorService executorService = Executors.newFixedThreadPool(5);

    public static void main(String... args) {
        final Producer producer = new Producer();
        executorService.submit(producer);
        executorService.shutdown();

        Observable<String> stringObservable = Observable.create(new Observable.OnSubscribe<String>() {
            @Override
            public void call(Subscriber<? super String> subscriber) {
                producer.register(subscriber);
            }
        });

        stringObservable.subscribe(new Subscriber<String>() {
            @Override
            public void onCompleted() {
                System.out.println("Completed");
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(String s) {
                System.out.println("Subscriber => " + s);
            }
        });

        stringObservable.subscribe(new Subscriber<String>() {
            @Override
            public void onCompleted() {
                System.out.println("Completed 2");
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(String s) {
                System.out.println("Subscriber 2 => " + s);
            }
        });

        Observable<Integer> integerObservable = stringObservable.map(new Func1<String, Integer>() {
            @Override
            public Integer call(String s) {
                if((new Random()).nextInt(10) == 1) {
                    throw new RuntimeException("Some sort of errors");
                }

                return Integer.parseInt(s);
            }
        });

        integerObservable.subscribe(new Subscriber<Integer>() {
            @Override
            public void onCompleted() {
                System.out.println("Completed Int");
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Integer integer) {
                System.out.println("Int Subscriber => " + integer);
            }
        });

        integerObservable.subscribe(new Subscriber<Integer>() {
            @Override
            public void onCompleted() {
                System.out.println("Completed Int 2");
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onNext(Integer integer) {
                System.out.println("Int Subscriber 2 => " + integer);
            }
        });
    }

    private static class Producer implements Runnable {
        private final List<Subscriber<? super String>> subscribers = Collections.synchronizedList(new ArrayList<>());
        private Boolean running = true;

        private void register(Subscriber<? super String> subscriber) {
            subscribers.add(subscriber);
        }

        @Override
        public void run() {
            while(running) {
                if((new Random()).nextInt(1000) == 1) {
                    running = false;
                    subscribers.forEach(Subscriber::onCompleted);
                }

                int val = (new Random()).nextInt();
                subscribers.forEach(s -> s.onNext(Integer.toString(val)));
            }
        }
    }

}
