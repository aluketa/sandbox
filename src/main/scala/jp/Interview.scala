package jp

object Interview extends App {
  println(findLongest("aabeeccccdd"))
  println(findLongest("aabeecccc"))
  println(findLongest("aaaaaabeeccccdd"))
  println(findLongest("aaacccc"))

  def findLongest(data: String): Char = {

    val resultMap: Map[Char, Int] = data.distinct.map(c => (c,data.count( x => x == c))).toMap
    //val maxCount = resultMap.values.max

    resultMap.filter(_._2 == resultMap.values.max).head._1

    /*val initialResult: Option[Result] = None
    val result: Option[Result] = data.toCharArray.toList.foldLeft(initialResult) { case (result: Option[Result], currentChar: Char) =>
      result match {
        case None =>
          Some(Result(currentChar, 1, currentChar, 1))
        case Some(r) if currentChar == r.previousChar && r.currentLength > r.resultLength =>
          Some(Result(currentChar, r.currentLength, currentChar, r.currentLength))
        case Some(r) if currentChar != r.previousChar =>
          Some(Result(currentChar, 1, r.result, r.resultLength))
        case Some(r) =>
          Some(Result(currentChar, r.currentLength + 1, r.result, r.resultLength))
      }
    }

    result.get.result*/
  }
}

case class Result(previousChar: Char, currentLength: Int, result: Char, resultLength: Int)
