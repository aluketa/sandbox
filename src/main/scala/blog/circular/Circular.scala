package blog.circular

object Circular extends App {

  lazy val fordMondeo = new Car("Ford", "Mondeo", sam)

  lazy val sam: Driver = new Driver("Sam", "Jones", fordMondeo)

  println(fordMondeo.getDriver)
  println(sam)

}

class Car(val make: String, val model: String, driver: => Driver) {
  def getDriver: Driver = this.driver
  //def driver: Driver = this.driver
}

class Driver(val firstName: String, val lastName: String, car: => Car) {
  def getCar: Car = this.car
}

/*
Handling circular dependencies in an immutable domain model

By its very nature, Scala encourages the use of immutable data types and we often find ourselves constructing our domain
model objects with immutability in mind. Indeed, it feels just plain wrong to declare a class variable as a 'var', while
providing setter methods to mutate internal state.

However, in certain situations it may be desirable for these immutable objects to depend on one another. In this blog
post I explore a mechanism for achieving this in Scala as well as exploring some of the pros and cons to this approach.



Let's take a (somewhat contrived) example where we may want to introduce a circular dependency into our domain model.
We will assume that we need an object to model a Car and an object to model a Driver. To simplify things, we can assume
that, at any given point in time, a Car will always have a Driver and that a Driver will always be driving a Car. There
will be a strict one-to-one relationship between these two entities.

We may define the Car first like so:
*/

//class Car(make: String, model: String)

/*
Now we might define the Driver like so:
*/

//class Driver(firstName: String, lastName: String)

/*
This is all good, however we need a way of associating a Car to a Driver, let's start by adding a reference to the
Car to the Driver class, so the definition of Driver becomes:
*/

//class Driver(firstName: String, lastName: String, car: Car)

/*
Given an instance of the Driver object, it is clear that it will be easy to obtain the associated Car. However, what
would happen if you only had a reference to a Car instance and you wanted to find the associated Driver? If you had
access to all of the Drivers then you might be able to find the one associated with your Car by searching this list like
so:
*/

object t1 {
  val myCar: Car = ???
  val allDrivers: Seq[Driver] = ???

  val theDriver: Option[Driver] = allDrivers.find(_.getCar == myCar)
}

/*
This would work, however there may be further problems. For example, the total list of drivers may not be readily
available at the point at which you need to obtain the Driver for a Car. Alternatively, the total number of drivers may
be extremely large and so there may be a performance penalty if this list is scanned each time to find a specific
Driver. Furthermore, the aforementioned solution returns an option meaning additional logic, and therefore complexity,
is required to handle the case where a Driver cannot be found.

One solution, and this may sometimes be preferable, would be to create and store a map of Car objects to Drivers. This
would resolve the performance problem of the lookup, however it may also add additional complexity to your codebase.

So long as the Driver and Car objects are created at the same time, it is possible to add circular references using lazy
initialisation. First, we update the definition of our Car and Driver classes like so:
*/

//class Car(val make: String, val model: String, driver: => Driver)

//class Driver(val firstName: String, val lastName: String, car: => Car)

/*
We declare the driver and car attributes 'call by name' rather than 'call by value', which is achieved by pre-pending the variable
type definition with the => symbol. This means that the values of driver and car will not be evaluated until they are
accessed. This allows us to define an instance of a Driver and an instance of Car and associate these with one another
using a forward reference (by forward reference we mean referencing a variable before it has been defined):
*/

object t2 {
  def createDataModel: (Seq[Driver], Seq[Car]) = {
    lazy val fordMondeo = new Car("Ford", "Mondeo", sam)
    lazy val vwGolf = new Car("VW", "Golf", david)

    lazy val sam: Driver = new Driver("Sam", "Jones", fordMondeo)
    lazy val david: Driver = new Driver("David", "Smith", vwGolf)

    (Seq(sam, david), Seq(fordMondeo, vwGolf))
  }
}

/*
Again, it is important to define the values as lazy so that they are not initialised until they are required. The driver
and the car attributes on the Car and Driver classes respectively, will be defined by the time either of the attributes
are required. The compiler will not allow you to declare a forward reference to a non-lazy value.
*/

/*
We are now able to define both Car and Driver objects and set references on these that are inter-dependent. However,
if we re-visit the class definition, we will notice that car attribute on the Driver class and the driver attribute on
the car class have <i>not</i> been declared as 'vals' and are therefore not accessible publically. This is because
'call by name' parameters may not be declared as vals as vals are non-lazy and will be resolved when the object
instance is initialised.

One final step, therefore, is to add accessors for the car and driver attributes so that they can be referenced
externally:
*/

/*
class Car(val make: String, val model: String, driver: => Driver) {
  def getDriver: Driver = this.driver
}

class Driver(val firstName: String, val lastName: String, car: => Car) {
  def getCar: Car = this.car
}*/

/*
Unfortunately, it is not possible to define the accessors with the same name as the attributes. This is because Scala
follows a strict implementation of the ???CHECK REFERENCE??? principal, which means parameter-less methods are
effectively the same thing as a member variable. In this example, I have used Java-style getters, however more
appropriate naming convention may be preferred.

Further to this, as all attributes in a 'case' class are automatically defined as 'vals', it is not possible to
implement this concept using case classes.
*/

/*
So, as we can see, using 'call by name' parameter definitions and forward referencing, allows us to define two or more
immutable objects that have inter-dependent relationships with one another. As mentioned, a consistent, simple and well
defined domain model is always preferred but there may be times where such circular dependencies allow for flexibility
and reduce complexity elsewhere in your code-base. This is yet another example of the rich tool-set provided by the
Scala programming language.
*/



/*
Let's take a (somewhat contrived) example where we may want to introduce a circular dependency into our domain model. We will assume that we need an object to model a Car and an object to model a Driver. To simplify things, we can assume that, at any given point in time, a Car will always have a Driver and that a Driver will always be driving a Car. There will be a strict one-to-one relationship between these two entities.
{br}{br}
We may define the Car first like so:
{code}
class Car(make: String, model: String)
{/code}
Now we might define the Driver like so:
{code}
class Driver(firstName: String, lastName: String)
{/code}
This is all good, however we need a way of associating a Car to a Driver, let's start by adding a reference to the Car to the Driver class, so the definition of Driver becomes:
{code}
class Driver(firstName: String, lastName: String, car: Car)
{/code}
Given an instance of the Driver object, it is clear that it will be easy to obtain the associated Car. However, what would happen if you only had a reference to a Car instance and you wanted to find the associated Driver? If you had access to all of the Drivers then you might be able to find the one associated with your Car by searching this list like so:
{code}
val myCar: Car = ???
val allDrivers: Seq[Driver] = ???

val theDriver: Option[Driver] = allDrivers.find(_.getCar == myCar)
{/code}
This would work, however there may be further problems. For example, the total list of drivers may not be readily available at the point at which you need to obtain the Driver for a Car. Alternatively, the total number of drivers may be extremely large and so there may be a performance penalty if this list is scanned each time to find a specific Driver. Furthermore, the aforementioned solution returns an option meaning additional logic, and therefore complexity, is required to handle the case where a Driver cannot be found.
{br}{br}
One solution, and this may sometimes be preferable, would be to create and store a map of Car objects to Drivers. This would resolve the performance problem of the lookup, however it may also add additional complexity to your codebase.
{br}{br}
So long as the Driver and Car objects are created at the same time, it is possible to add circular references using lazy initialisation. First, we update the definition of our Car and Driver classes like so:
{code}
class Car(val make: String, val model: String, driver: => Driver)

class Driver(val firstName: String, val lastName: String, car: => Car)
{/code}

We declare the driver and car attributes 'call by name' rather than 'call by value', which is achieved by pre-pending the variable type definition with the => symbol. This means that the values of driver and car will not be evaluated until they are accessed. This allows us to define an instance of a Driver and an instance of Car and associate these with one another using a forward reference (by forward reference we mean referencing a variable before it has been defined):
{code}
def createDataModel: (Seq[Driver], Seq[Car]) = {
  lazy val fordMondeo = new Car("Ford", "Mondeo", sam)
  lazy val vwGolf = new Car("VW", "Golf", david)

  lazy val sam: Driver = new Driver("Sam", "Jones", fordMondeo)
  lazy val david: Driver = new Driver("David", "Smith", vwGolf)

  (Seq(sam, david), Seq(fordMondeo, vwGolf))
}
{/code}

Again, it is important to define the values as lazy so that they are not initialised until they are required. The driver and the car attributes on the Car and Driver classes respectively, will be defined by the time either of the attributes are required. The compiler will not allow you to declare a forward reference to a non-lazy value.
{br}{br}
We are now able to define both Car and Driver objects and set references on these that are inter-dependent. However, if we re-visit the class definition, we will notice that car attribute on the Driver class and the driver attribute on the car class have <i>not</i> been declared as 'vals' and are therefore not accessible publically. This is because 'call by name' parameters may not be declared as vals as vals are non-lazy and will be resolved when the object instance is initialised.
{br}{br}
One final step, therefore, is to add accessors for the car and driver attributes so that they can be referenced externally:
{code}
class Car(val make: String, val model: String, driver: => Driver) {
  def getDriver: Driver = this.driver
}

class Driver(val firstName: String, val lastName: String, car: => Car) {
  def getCar: Car = this.car
}
{/code}
Unfortunately, it is not possible to define the accessors with the same name as the attributes. This is because Scala follows a strict implementation of the ???CHECK REFERENCE??? principal, which means parameter-less methods are effectively the same thing as a member variable. In this example, I have used Java-style getters, however more appropriate naming convention may be preferred.
{br}{br}
Further to this, as all attributes in a 'case' class are automatically defined as 'vals', it is not possible to implement this concept using case classes.
{br}{br}
So, as we can see, using 'call by name' parameter definitions and forward referencing, allows us to define two or more immutable objects that have inter-dependent relationships with one another. As mentioned, a consistent, simple and well defined domain model is always preferred but there may be times where such circular dependencies allow for flexibility and reduce complexity elsewhere in your code-base. This is yet another example of the rich tool-set provided by the Scala programming language.
*/