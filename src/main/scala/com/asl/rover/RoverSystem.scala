package com.asl.rover

class RoverSystem {
  def processCommands(commandString: String): String = {
    val commands = commandString.split('\n')

    commands.drop(1).sliding(2, 2)
      .map(data => commandRover(data(0), data(1)))
      .map(_.toString)
      .mkString("\n")
  }

  private def commandRover(positionString: String, commandString: String): Position = {
    val currentPosition = new Position(positionString)
    commandString.toCharArray.foldLeft(currentPosition) {
      case (position, command) => processCommand(command, position)
    }
  }

  private def processCommand(command: Char, position: Position): Position = command match {
    case 'L' => moveLeft(position)
    case 'R' => moveRight(position)
    case 'M' => move(position)
    case _ => throw new RuntimeException(s"Unknown command: $command")
  }

  private def moveLeft(position: Position): Position = position.direction match {
    case 'N' => position.copy(direction = 'W')
    case 'W' => position.copy(direction = 'S')
    case 'S' => position.copy(direction = 'E')
    case 'E' => position.copy(direction = 'N')
    case _ => throw new RuntimeException(s"Unknown direction: ${position.direction}")
  }

  private def moveRight(position: Position): Position = position.direction match {
    case 'N' => position.copy(direction = 'E')
    case 'W' => position.copy(direction = 'N')
    case 'S' => position.copy(direction = 'W')
    case 'E' => position.copy(direction = 'S')
    case _ => throw new RuntimeException(s"Unknown direction: ${position.direction}")
  }

  private def move(position: Position): Position = position.direction match {
    case 'N' => position.copy(y = position.y + 1)
    case 'S' => position.copy(y = position.y - 1)
    case 'E' => position.copy(x = position.x + 1)
    case 'W' => position.copy(x = position.x - 1)
    case _ => throw new RuntimeException(s"Unknown direction: ${position.direction}")
  }

  private case class Position(x: Int, y: Int, direction: Char) {
    def this(positionString: String) = {
      this(
        positionString.split(' ')(0).toInt,
        positionString.split(' ')(1).toInt,
        positionString.split(' ')(2).toCharArray()(0))
    }

    override def toString = s"$x $y $direction"
  }
}