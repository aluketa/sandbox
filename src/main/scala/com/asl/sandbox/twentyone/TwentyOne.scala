package com.asl.sandbox.twentyone

object TwentyOne extends App {

  final val Bust = 99
  final val MinimumBet = 5
  val deck = new SingleDeck
  val game = new Game(deck, 13, 17)

  var pot = 0

  for (i <- 1 to 10000) {
    val outcome = game.play
    outcome match {
      case Outcome.PlayerWins => pot += (MinimumBet * 2)
      case Outcome.DealerWins => pot -= MinimumBet
      case Outcome.Draw => pot += MinimumBet
    }
  }

  println(s"Pot amount: $pot")

}
