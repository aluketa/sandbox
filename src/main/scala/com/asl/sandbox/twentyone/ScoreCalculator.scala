package com.asl.sandbox.twentyone

object ScoreCalculator {
  def calculateScores(cards: Seq[Card]): Seq[Int] = cards match {
    case Seq() => Seq()
    case _ => {
      val card = cards.head
      val scores: Seq[Seq[Int]] = cs(Seq(Seq(card.values._1), Seq(card.values._2)), cards.tail)
      scores.map(_.sum).distinct
    }
  }

  private[this] def cs(results: Seq[Seq[Int]], cards: Seq[Card]): Seq[Seq[Int]] = {
    cards match {
      case Seq() => results
      case _ => {
        val card = cards.head
        val nextResults: Seq[Seq[Int]] = results flatMap { r => Seq(r :+ card.values._1, r :+ card.values._2) }
        cs(nextResults, cards.tail)
      }
    }
  }
}
