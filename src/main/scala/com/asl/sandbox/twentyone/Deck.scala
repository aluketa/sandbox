package com.asl.sandbox.twentyone

import scala.util.Random

trait Deck {
  def deal: Card
}

class SingleDeck extends Deck {
  private val cards: Seq[Card] = buildDeck
  private var cardsIterator: Iterator[Seq[Card]] = buildIterator

  override def deal: Card = {
    if(!cardsIterator.hasNext) {
      cardsIterator = buildIterator
    }
    cardsIterator.next().head
  }

  private def buildDeck: Seq[Card] = {
    val numbers: Seq[Card] = for(n <- 2 to 10; suit <- Suit.values) yield Card(n.toString, suit, (n, n))
    val pictures: Seq[Card] = for(n <- Seq("King", "Queen", "Jack"); suit <- Suit.values) yield Card(n, suit, (10, 10))
    val aces: Seq[Card] = (for(suit <- Suit.values) yield Card("Ace", suit, (1, 11))).toSeq

    numbers ++ pictures ++ aces
  }

  private def buildIterator: Iterator[Seq[Card]] = Random.shuffle(cards).sliding(1)
}

case class Card(name: String, suit: Suit.Suit, values: Pair[Int, Int])

object Suit extends Enumeration {
  type Suit = Value
  val Club, Spade, Heart, Diamond = Value
}