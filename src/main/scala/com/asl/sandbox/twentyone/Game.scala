package com.asl.sandbox.twentyone

import scala.collection.mutable.ListBuffer
import com.asl.sandbox.twentyone.ScoreCalculator._

object Game {
  final val Bust = 99
}

class Game(deck: Deck, dealerHitCutOff: Int = 13, playerHitCutOff: Int = 13) {
  import Outcome.{Outcome, DealerWins, PlayerWins, Draw}
  import Game.Bust

  def play: Outcome = {
    val dealerScore: Int = play(Seq(deck.deal, deck.deal), dealerHitCutOff)
    val playerScore: Int = play(Seq(deck.deal, deck.deal), playerHitCutOff)

    if (playerScore == Bust)
      DealerWins
    else if (dealerScore == Bust)
      PlayerWins
    else if (dealerScore > playerScore)
      DealerWins
    else if (playerScore > dealerScore)
      PlayerWins
    else if (playerScore == dealerScore)
      Draw
    else
      throw new RuntimeException(s"Unknown outcome: Player Score: $playerScore, Dealer Score: $dealerScore")
  }

  private[this] def play(cards: Seq[Card], hitCutOff: Int): Int = {
    val hand = new ListBuffer[Card]
    hand.appendAll(cards)
    while (calculateScores(hand).min <= hitCutOff) {
      hand.append(deck.deal)
    }

    calculateScores(hand).filter(_ <= 21) match {
      case Seq() => Bust
      case s => s.max
    }
  }
}

object Outcome extends Enumeration {
  type Outcome = Value
  val DealerWins, PlayerWins, Draw = Value
}