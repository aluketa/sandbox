package com.asl.encryption

import java.io._
import javax.crypto.Cipher
import javax.crypto.spec.{IvParameterSpec, SecretKeySpec}

object Encryption {
  private val SecretKey = new SecretKeySpec(Array(2,-19,27,32,35,15,21,-16,-78,23,-321,-500,23,75,-32,-43).map(_.toByte), "AES")
  private val IvParameterSpec = new IvParameterSpec(Array(22,-42,-54,-63,11,112,14,-23,-66,-40,-21,-6,-12,52,40,-20).map(_.toByte))
  private val AesCipher = Cipher.getInstance("AES/CTR/NoPadding")

  def encrypt(data: Array[Byte]): Array[Byte] = {
    AesCipher.init(Cipher.ENCRYPT_MODE, SecretKey, IvParameterSpec)
    AesCipher.doFinal(data)
  }

  def decrypt(data: Array[Byte]): Array[Byte] = {
    AesCipher.init(Cipher.DECRYPT_MODE, SecretKey, IvParameterSpec)
    AesCipher.doFinal(data)
  }
}

trait Encryption extends OutputStream {
  abstract override def write(byteArray: Array[Byte], off: Int, len: Int) {
    super.write(Encryption.encrypt(byteArray), off, len)
  }

  override def write(b: Array[Byte]) {
    this.write(b, 0, b.length)
  }
}

trait Decryption extends InputStream {
  abstract override def read(byteArray: Array[Byte], off: Int, len: Int): Int = {
    val resultLength = super.read(byteArray, off, len)
    if(resultLength > -1) {
      val decryptedArray = Encryption.decrypt(byteArray)
      System.arraycopy(decryptedArray, 0, byteArray, 0, resultLength)
    }

    resultLength
  }

  override def read(b: Array[Byte]): Int = this.read(b, 0, b.length)
}

object FileEncrypter {

  def main(args: Array[String]) {
    val sourceFile = args.head
    val destFilename = args.head + ".enc"

    val fis = new FileInputStream(sourceFile)
    val fos = new FileOutputStream(destFilename) with Encryption

    val buffer = new Array[Byte](4096)
    var n = fis.read(buffer)
    while(n > -1) {
      fos.write(buffer, 0, n)
      n = fis.read(buffer)
    }

    fis.close()
    fos.close()
  }

}

object FileDecrypter {

  def main(args: Array[String]) {
    val sourceFile = args.head + ".enc"
    val destFile = args.head + ".dec"

    val fis = new FileInputStream(sourceFile) with Decryption
    val fos = new FileOutputStream(destFile)

    val buffer = new Array[Byte](4096)
    var n = fis.read(buffer)
    while(n > -1) {
      fos.write(buffer, 0, n)
      n = fis.read(buffer)
    }

    fis.close()
    fos.close()
  }

}

object Tester extends App {
  val fos = new ObjectOutputStream(new FileOutputStream("/var/tmp/some_file"))
  fos.writeObject("String data")

  val fis = new ObjectInputStream(new FileInputStream("/var/tmp/some_file"))
  val data = fis.readObject().asInstanceOf[String]
}

/*

Adding Encryption/Decryption to Java Output/Input Streams using Scala Mixins

Scala traits allow functionality to be applied to existing implementations in a 'stackable' fashion by utilising the
<a href="http://en.wikipedia.org/wiki/Mixin">mixin pattern</a>. In this post, I explore a use of this pattern that
allows encryption and decryption to be applied to existing implementations of Java Output and Input Streams.

The OutputStream and InputStream implementations within the standard Java library are excellent examples of the
<a href="http://en.wikipedia.org/wiki/Decorator_pattern">Decorator Pattern</a>, with various implementations allowing
functionality to be stacked according to need. For example, the following code can be used to allow objects to be
serialized and written out to a file:

{code}
val fos = new ObjectOutputStream(new FileOutputStream("/var/tmp/some_file"))
fos.writeObject("String data")
{/code}

It is also possible to write your own implementations that add additional functionality and then delegate the call
to an existing instance. For example, presuming the existence of an EncryptionOutputStream class, the following
logic might be used to add encryption to the previous example:

{code}
val fos = new EncryptedOutputStream(new ObjectOutputStream(new FileOutputStream("/var/tmp/some_file")))
fos.writeObject("String data")
{/code}

The decorator pattern, however, does have some limitations. For example, the interface may be complex with lots of
methods that will need to be implemented with the calls delegated to the underlying object that is being decorated.
Indeed, while interfaces should follow the <a href="http://en.wikipedia.org/wiki/Interface_segregation_principle">
Interface Segregation Principal</a>, existing legacy code and even some of the key Java library interfaces define
complicated interfaces that are a pain to implement when applying the decorator pattern.

While the five methods on OutputStream and nine methods on InputStream are not exactly onerous, they can involve
writing more code than is necessary if, for example, a given implementation only requires unique definitions for
a small subset of the methods.

Mixins implemented using Scala traits offer a clever way of decorating existing functionality in a stackable fashion,
similar to the decorator pattern, without the need for additional boilerplate implementations for all methods.

I recently had a need to add some basic encryption to some existing output and input streams and, rather than implement
this using the traditional decorator style, I thought it would be useful to explore an implementation using stackable
traits instead.

The goal is to implement a trait that allows the following logic to be applied in order to add encryption to the
previous example:

{code}
val fos = new ObjectOutputStream(new FileOutputStream("/var/tmp/some_file")) <b>with Encryption</b>
fos.writeObject("String data")
{/code}

It should also be possible to decrypt InputStreams in a similar fashion:

{code}
val fis = new ObjectInputStream(new FileInputStream("/var/tmp/some_file")) <b>with Decryption</b>
val data = fis.readObject().asInstanceOf[String]
{/code}

The solution that was devised for this is as follows:

{code}
trait Encryption extends OutputStream {
  abstract override def write(byteArray: Array[Byte], off: Int, len: Int) {
    super.write(Encryption.encrypt(byteArray), off, len)
  }

  override def write(b: Array[Byte]) {
    this.write(b, 0, b.length)
  }
}

trait Decryption extends InputStream {
  abstract override def read(byteArray: Array[Byte], off: Int, len: Int): Int = {
    val resultLength = super.read(byteArray, off, len)
    if(resultLength > -1) {
      val decryptedArray = Encryption.decrypt(byteArray)
      System.arraycopy(decryptedArray, 0, byteArray, 0, resultLength)
    }

    resultLength
  }

  override def read(b: Array[Byte]): Int = this.read(b, 0, b.length)
}
{/code}

The first thing to note about the Encryption and Decryption traits is that they extend OutputStream and InputStream
respectively. This means that the trait can only be mixed-in with instances that implement the given interfaces.

Secondly, the overridden methods are defined as abstract, which allows a super call to be made that will be dynamically
bound to the implementation on the instance that this trait is being mixed into. What this means is that our Encryption
method will be called before the implementation on the instance we mixing into. We can then delegate to the instance
implementation via the call to super. This allows the existing functionality of the streams to be preserved while
allowing the data to be intercepted so that it can be encrypted or decrypted accordingly.

One final thing to add is the definition of the Encryption companion object, which provides basic encryption and
decryption implementations using the AES algorithm:

{code}
object Encryption {
  private val SecretKey = new SecretKeySpec(Array(2,-19,27,32,35,15,21,-16,-78,23,-321,-500,23,75,-32,-43).map(_.toByte), "AES")
  private val IvParameterSpec = new IvParameterSpec(Array(22,-42,-54,-63,11,112,14,-23,-66,-40,-21,-6,-12,52,40,-20).map(_.toByte))
  private val AesCipher = Cipher.getInstance("AES/CTR/NoPadding")

  def encrypt(data: Array[Byte]): Array[Byte] = {
    AesCipher.init(Cipher.ENCRYPT_MODE, SecretKey, IvParameterSpec)
    AesCipher.doFinal(data)
  }

  def decrypt(data: Array[Byte]): Array[Byte] = {
    AesCipher.init(Cipher.DECRYPT_MODE, SecretKey, IvParameterSpec)
    AesCipher.doFinal(data)
  }
}
{/code}

(Please note that the above code should be used with caution, it has not been rigorously performance tested and is not thread safe.)
*/