package com.asl.enigma.en1766

/*
* I have listed in random order five positive integers, four of two digits and one of one digit. They use each of the
* digits 1 to 9. None of the two-digit integers has any factor greater than 1 in common with any other of the two-digit
* integers. The first of the integers in my list is a triangular number. The sum of the first two, the sum of the first
* three, the sum of the first four and the sum of all five are also triangular numbers. What are my integers in the
* order in which I have listed them?
*
* 6, 49, 23, 58, 17
*
* */

object Triangular extends App {
  val triangularNumbers: Set[Int] = deriveTriangularNumbers(99)

  val twoDigitNumbers: Seq[Int] = 10 to 99

  val twoDigitCombinations: Seq[Seq[Int]] = (twoDigitNumbers.combinations(4) filter (x => noCommonFactorsGreaterThanOne(x))).toSeq

  twoDigitCombinations foreach { td =>
    (1 to 10) foreach { sn =>
      test(td, sn)
    }
  }

  private def test(tdNumbers: Seq[Int], sn: Int) {
    val numbersToTest: Seq[Int] = sn +: tdNumbers

    if (testOneToNine(numbersToTest)) {
      numbersToTest.permutations foreach { candidate: Seq[Int] =>
        if (testTriangularNumbers(candidate)) {
          println(candidate)
        }
      }
    }
  }

  private def testTriangularNumbers(candidate: Seq[Int]): Boolean =
    (1 to 5) forall (n => triangularNumbers.contains(candidate.take(n).sum))

  private def noCommonFactorsGreaterThanOne(tdNumbers: Seq[Int]): Boolean = {
    val factors: Seq[Int] = tdNumbers flatMap (n => factorsGreaterThanOne(n))
    factors == factors.distinct
  }

  private def factorsGreaterThanOne(n: Int): Seq[Int] = (2 to n) filter ( x => n % x == 0)

  private def testOneToNine(ns: Seq[Int]): Boolean = ns.mkString.toCharArray.sortBy(x => x).mkString == "123456789"

  private def deriveTriangularNumbers(n: Int): Set[Int] = ((1 to n) map (i => (i * (i + 1)) / 2 )).toSet

}
