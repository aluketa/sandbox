package com.asl.enigma.en1765

object AnswerDeterminer {

  def isAnswer(n: Int): Boolean = {
    val digits: Seq[Int] = n.toString.map(_.toString.toInt)
    digits.sliding(2).forall(x => x(1) > x(0))
  }

}
