package com.asl.enigma.en1765

import scala.collection.mutable.{Map => MutableMap}

/*
* I have before me some positive whole numbers, each consisting of a single digit, which may be repeated. The digit is
* different for each number, and the number of times it is repeated is also different for each number.
* The sum of my numbers is a number in which each digit is larger than the digit on its left, and it is the largest
* number for which this is possible, given the constraints described above.
*
* What is the sum of my numbers?
*
* */

object Program extends App {
  val answers = MutableMap[Int, Seq[Int]]()
  val digits = Seq(1, 2, 3, 4, 5, 6, 7, 8, 9)

  (2 to 9) foreach { i =>
    digits.combinations(i) foreach { digit =>
      digits.combinations(i) foreach { repetitions =>
        repetitions.permutations foreach { r =>
          val components = applyRepetitions(digit, r)
          if (AnswerDeterminer.isAnswer(components.sum)) {
            answers.put(components.sum, components)
          }
        }
      }
    }
  }

  val maxAnswer: Int = answers.keys.max
  println(maxAnswer + " => " + answers(maxAnswer))

  private def applyRepetitions(digits: Seq[Int], repetitions: Seq[Int]): Seq[Int] = {
    digits.zip(repetitions) map { case (d, r) => "".padTo(r, d).mkString.toInt}
  }
}