package wordwrap

object WordWrap {
  implicit def convertStringToWordWrap(s: String): WordWrap = new WordWrap(s)
}

class WordWrap(s: String) {
  def wordWrap(column: Int): String = wordWrap(s, column)

  def wordWrap(currentString: String, column: Int): String = {
    if (currentString == "") {
      ""
    }
    else if (currentString.contains(' ')) {
      val spaceAt = currentString.substring(column).lastIndexOf(' ')
      currentString(spaceAt) + "\n" + wordWrap(currentString.substring(spaceAt, currentString.length), column)
    }
    else {
      currentString
    }
  }
}
