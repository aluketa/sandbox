package tennis

class TennisGame(playerOne: Player, playerTwo: Player) {
  private val scoreMap: Map[Int, String] = Map(0 -> "love", 1 -> "fifteen", 2 -> "thirty", 3 -> "forty")

  def score: String = (playerOne, playerTwo) match {
    case (p1, _)  if bothPlayersReach40 && advantagePlayerOne => s"advantage ${p1.name}"
    case (_, p2)  if bothPlayersReach40 && advantagePlayerTwo => s"advantage ${p2.name}"
    case _        if bothPlayersReach40 && deuce              => "deuce"
    case (p1, _)  if playerOneWins                            => s"${p1.name} won"
    case (_, p2)  if playerTwoWins                            => s"${p2.name} won"
    case _ => Seq(playerOne, playerTwo).map(p => scoreMap(p.score)).mkString(", ")
  }



  private def bothPlayersReach40 = playerOne.score >= 3 && playerTwo.score >= 3
  private def advantagePlayerOne = playerOne.score == playerTwo.score + 1
  private def advantagePlayerTwo = playerTwo.score == playerOne.score + 1
  private def deuce = playerOne.score == playerTwo.score
  private def playerOneWins = playerOne.score >= 4 && playerOne.score >= playerTwo.score + 2
  private def playerTwoWins = playerTwo.score >= 4 && playerTwo.score >= playerOne.score + 2
}

case class Player(name: String) {
  var score: Int = 0

  def winBall = score += 1
}
