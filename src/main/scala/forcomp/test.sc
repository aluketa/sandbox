import forcomp.Anagrams._
val occurrences: Occurrences = sentenceOccurrences(List("on"))
val combs: List[Occurrences] = combinations(occurrences)

val words: Map[Occurrences, List[Word]] =
  combs.map(x => (x, dictionaryByOccurrences.get(x))).toMap.mapValues(_.getOrElse(List()))






/*combs foreach { c =>
  dictionaryByOccurrences.get(c) match {
    case Some(w) => println(c + " => " + w)
    case None => println(c + " => None")
  }
}*/






