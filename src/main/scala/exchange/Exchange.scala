package exchange

import exchange.Directions.{Buy, Direction, Sell}

class Exchange {

  private var openOrders: Seq[Order] = Seq()
  private var executedOrders: Seq[Order] = Seq()
  private var lastPrice: Map[String, BigDecimal] = Map()

  def add(order: Order): Unit = matchOrder(order) match {
    case Some(matchedOrder) =>
      openOrders = openOrders.filter(_ != matchedOrder)
      executedOrders = executedOrders ++ execute(order, matchedOrder)
    case None => openOrders = openOrders :+ order
  }

  private def matchOrder(order: Order): Option[Order] = {
    openOrders
      .filter(_.matches(order))
      .sortWith(sortBy(order.direction))
      .headOption
  }

  private def sortBy(orderDirection: Direction)(order1: Order, order2: Order): Boolean = {
    orderDirection match {
      case Buy => order1.price < order2.price
      case Sell => order1.price > order2.price
    }
  }

  private def execute(order: Order, matched: Order): Seq[Order] = {
    lastPrice = lastPrice + (order.ric -> order.price)

    Seq(
      order.copy(executionPrice = Some(order.price)),
      matched.copy(executionPrice = Some(order.price)))
  }

  def cancel(order: Order): Unit = openOrders = openOrders.filter(_ != order)

  def openOrdersFor(user: String): Seq[Order] = openOrders.filter(_.user == user)

  def executedOrdersFor(user: String): Seq[Order] = executedOrders.filter(_.user == user)

  def lastPriceOf(ric: String): Option[BigDecimal] = lastPrice.get(ric)
}

case class Order(
    direction: Direction,
    ric: String,
    quantity: Long,
    price: BigDecimal,
    user: String,
    executionPrice: Option[BigDecimal] = None) {

  def matches(other: Order): Boolean =
    other.direction != direction &&
      other.ric == ric &&
      other.quantity == quantity &&
      priceMatch(other)

  private def priceMatch(other: Order): Boolean = direction match {
    case Buy => other.price <= price
    case Sell => other.price >= price
  }
}

object Directions {
  sealed trait Direction
  case object Buy extends Direction
  case object Sell extends Direction
}