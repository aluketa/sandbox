package exchange

import exchange.Directions.{Buy, Sell}
import org.specs2.mutable.Specification

class ExchangeSpec extends Specification {

  "The Exchange System" should {

    "add a single order" >> {
      val exchange = new Exchange
      exchange.add(Order(Buy, "VOD.L", 1000, 100, "test"))
      exchange.openOrdersFor("test") must beEqualTo(Seq(Order(Buy, "VOD.L", 1000, 100, "test")))
    }

    "execute an order with same price" >> {
      val exchange = new Exchange
      exchange.add(Order(Buy, "VOD.L", 1000, 100, "test1"))
      exchange.add(Order(Sell, "VOD.L", 1000, 100, "test2"))

      exchange.openOrdersFor("test1") must beEmpty
      exchange.openOrdersFor("test2") must beEmpty
      exchange.executedOrdersFor("test1") must beEqualTo(Seq(
        Order(Buy, "VOD.L", 1000, 100, "test1", Some(100))
      ))
      exchange.executedOrdersFor("test2") must beEqualTo(Seq(
        Order(Sell, "VOD.L", 1000, 100, "test2", Some(100))
      ))
    }

    "execute a sell order with buy price higher than sell using execution price of newly added order" >> {
      val exchange = new Exchange
      exchange.add(Order(Buy, "VOD.L", 1000, 101, "test1"))
      exchange.add(Order(Sell, "VOD.L", 1000, 99, "test2"))

      exchange.openOrdersFor("test1") must beEmpty
      exchange.openOrdersFor("test2") must beEmpty
      exchange.executedOrdersFor("test1") must beEqualTo(Seq(
        Order(Buy, "VOD.L", 1000, 101, "test1", Some(99))
      ))
      exchange.executedOrdersFor("test2") must beEqualTo(Seq(
        Order(Sell, "VOD.L", 1000, 99, "test2", Some(99))
      ))
    }

    "execute a buy order with buy price higher than sell using execution price of newly added order" >> {
      val exchange = new Exchange
      exchange.add(Order(Sell, "VOD.L", 1000, 99, "test1"))
      exchange.add(Order(Buy, "VOD.L", 1000, 101, "test2"))

      exchange.openOrdersFor("test1") must beEmpty
      exchange.openOrdersFor("test2") must beEmpty
      exchange.executedOrdersFor("test1") must beEqualTo(Seq(
        Order(Sell, "VOD.L", 1000, 99, "test1", Some(101))
      ))
      exchange.executedOrdersFor("test2") must beEqualTo(Seq(
        Order(Buy, "VOD.L", 1000, 101, "test2", Some(101))
      ))
    }

    "not execute an order if sell price is higher than buy" >> {
      val exchange = new Exchange
      exchange.add(Order(Buy, "VOD.L", 1000, 99, "test1"))
      exchange.add(Order(Sell, "VOD.L", 1000, 101, "test2"))

      exchange.openOrdersFor("test1") must beEqualTo(Seq(Order(Buy, "VOD.L", 1000, 99, "test1")))
      exchange.openOrdersFor("test2") must beEqualTo(Seq(Order(Sell, "VOD.L", 1000, 101, "test2")))
      exchange.executedOrdersFor("test1") must beEmpty
    }

    "not execute an order if both have same direction" >> {
      val exchange = new Exchange
      exchange.add(Order(Buy, "VOD.L", 1000, 100, "test1"))
      exchange.add(Order(Buy, "VOD.L", 1000, 100, "test2"))

      exchange.openOrdersFor("test1") must beEqualTo(Seq(Order(Buy, "VOD.L", 1000, 100, "test1")))
      exchange.openOrdersFor("test2") must beEqualTo(Seq(Order(Buy, "VOD.L", 1000, 100, "test2")))
      exchange.executedOrdersFor("test1") must beEmpty
    }

    "not execute an order if both have different ric codes" >> {
      val exchange = new Exchange
      exchange.add(Order(Buy, "VOD.L", 1000, 100, "test1"))
      exchange.add(Order(Sell, "HSBC.L", 1000, 100, "test2"))

      exchange.openOrdersFor("test1") must beEqualTo(Seq(Order(Buy, "VOD.L", 1000, 100, "test1")))
      exchange.openOrdersFor("test2") must beEqualTo(Seq(Order(Sell, "HSBC.L", 1000, 100, "test2")))
      exchange.executedOrdersFor("test1") must beEmpty
    }

    "not execute an order if both have different quantities" >> {
      val exchange = new Exchange
      exchange.add(Order(Buy, "VOD.L", 1000, 100, "test1"))
      exchange.add(Order(Sell, "VOD.L", 2000, 100, "test2"))

      exchange.openOrdersFor("test1") must beEqualTo(Seq(Order(Buy, "VOD.L", 1000, 100, "test1")))
      exchange.openOrdersFor("test2") must beEqualTo(Seq(Order(Sell, "VOD.L", 2000, 100, "test2")))
      exchange.executedOrdersFor("test1") must beEmpty
    }

    "not re-use already matched orders in subsequent executions" >> {
      val exchange = new Exchange
      exchange.add(Order(Buy, "VOD.L", 1000, 100, "test1"))
      exchange.add(Order(Sell, "VOD.L", 1000, 100, "test2"))
      exchange.add(Order(Sell, "VOD.L", 1000, 100, "test3"))

      exchange.openOrdersFor("test1") must beEmpty
      exchange.openOrdersFor("test2") must beEmpty
      exchange.openOrdersFor("test3") must beEqualTo(Seq(Order(Sell, "VOD.L", 1000, 100, "test3")))
    }

    "execute a buy order with multiple matches with different prices at the correct price" >> {
      val exchange = new Exchange
      exchange.add(Order(Sell, "VOD.L", 1000, 99, "test1"))
      exchange.add(Order(Sell, "VOD.L", 1000, 98, "test2"))
      exchange.add(Order(Buy, "VOD.L", 1000, 100, "test3"))

      exchange.openOrdersFor("test1") must beEqualTo(Seq(Order(Sell, "VOD.L", 1000, 99, "test1")))
      exchange.openOrdersFor("test2") must beEmpty
      exchange.openOrdersFor("test3") must beEmpty

      exchange.executedOrdersFor("test1") must beEmpty
      exchange.executedOrdersFor("test2") must beEqualTo(Seq(Order(Sell, "VOD.L", 1000, 98, "test2", Some(100))))
      exchange.executedOrdersFor("test3") must beEqualTo(Seq(Order(Buy, "VOD.L", 1000, 100, "test3", Some(100))))
    }

    "execute a sell order with multiple matches with different prices at the correct price" >> {
      val exchange = new Exchange
      exchange.add(Order(Buy, "VOD.L", 1000, 98, "test1"))
      exchange.add(Order(Buy, "VOD.L", 1000, 99, "test2"))
      exchange.add(Order(Sell, "VOD.L", 1000, 95, "test3"))

      exchange.openOrdersFor("test1") must beEqualTo(Seq(Order(Buy, "VOD.L", 1000, 98, "test1")))
      exchange.openOrdersFor("test2") must beEmpty
      exchange.openOrdersFor("test3") must beEmpty

      exchange.executedOrdersFor("test1") must beEmpty
      exchange.executedOrdersFor("test2") must beEqualTo(Seq(Order(Buy, "VOD.L", 1000, 99, "test2", Some(95))))
      exchange.executedOrdersFor("test3") must beEqualTo(Seq(Order(Sell, "VOD.L", 1000, 95, "test3", Some(95))))
    }

    "execute an order with multiple matches with same price picking earliest order to match" >> {
      val exchange = new Exchange
      exchange.add(Order(Buy, "VOD.L", 1000, 100, "test2"))
      exchange.add(Order(Buy, "VOD.L", 1000, 100, "test1"))
      exchange.add(Order(Sell, "VOD.L", 1000, 100, "test3"))

      exchange.openOrdersFor("test1") must beEqualTo(Seq(Order(Buy, "VOD.L", 1000, 100, "test1")))
      exchange.openOrdersFor("test2") must beEmpty
      exchange.openOrdersFor("test3") must beEmpty

      exchange.executedOrdersFor("test1") must beEmpty
      exchange.executedOrdersFor("test2") must beEqualTo(Seq(Order(Buy, "VOD.L", 1000, 100, "test2", Some(100))))
      exchange.executedOrdersFor("test3") must beEqualTo(Seq(Order(Sell, "VOD.L", 1000, 100, "test3", Some(100))))
    }

    "cancel an open order" >> {
      val exchange = new Exchange
      exchange.add(Order(Buy, "VOD.L", 1000, 100, "test"))
      exchange.cancel(Order(Buy, "VOD.L", 1000, 100, "test"))

      exchange.openOrdersFor("test") must beEmpty
    }

    "list open orders for a given user" >> {
      val exchange = new Exchange
      exchange.add(Order(Buy, "VOD.L", 1000, 100, "test1"))
      exchange.add(Order(Buy, "HSBC.L", 1000, 100, "test2"))

      exchange.openOrdersFor("test1") must beEqualTo(Seq(Order(Buy, "VOD.L", 1000, 100, "test1")))
      exchange.openOrdersFor("test2") must beEqualTo(Seq(Order(Buy, "HSBC.L", 1000, 100, "test2")))
    }

    "list executed orders for a given user" >> {
      val exchange = new Exchange
      exchange.add(Order(Buy, "VOD.L", 1000, 100, "test1"))
      exchange.add(Order(Sell, "VOD.L", 1000, 100, "test2"))

      exchange.executedOrdersFor("test1") must beEqualTo(Seq(Order(Buy, "VOD.L", 1000, 100, "test1", Some(100))))
      exchange.executedOrdersFor("test2") must beEqualTo(Seq(Order(Sell, "VOD.L", 1000, 100, "test2", Some(100))))
    }

    "return the last price for a given underlying" >> {
      val exchange = new Exchange
      exchange.add(Order(Buy, "VOD.L", 1000, 100, "test1"))
      exchange.add(Order(Sell, "VOD.L", 1000, 100, "test2"))

      exchange.lastPriceOf("VOD.L") must beSome(100)

      exchange.add(Order(Buy, "VOD.L", 1000, 50, "test1"))
      exchange.add(Order(Sell, "VOD.L", 1000, 50, "test2"))

      exchange.lastPriceOf("VOD.L") must beSome(50)

      exchange.add(Order(Buy, "VOD.L", 1000, 200, "test1"))
      exchange.add(Order(Sell, "VOD.L", 1000, 200, "test2"))

      exchange.lastPriceOf("VOD.L") must beSome(200)
    }

    "return the execution price as the last price for a given underlying" >> {
      val exchange = new Exchange
      exchange.add(Order(Buy, "VOD.L", 1000, 110, "test1"))
      exchange.add(Order(Sell, "VOD.L", 1000, 100, "test2"))

      exchange.lastPriceOf("VOD.L") must beSome(100)
    }

    "return the last price for different underlyings" >> {
      val exchange = new Exchange
      exchange.add(Order(Buy, "VOD.L", 1000, 100, "test1"))
      exchange.add(Order(Sell, "VOD.L", 1000, 100, "test2"))

      exchange.add(Order(Buy, "HSBC.L", 1000, 50, "test1"))
      exchange.add(Order(Sell, "HSBC.L", 1000, 50, "test2"))

      exchange.lastPriceOf("VOD.L") must beSome(100)
      exchange.lastPriceOf("HSBC.L") must beSome(50)
    }

    "return None for the last price of an unknown underlying" >> {
      val exchange = new Exchange
      exchange.lastPriceOf("UNK") must beNone
    }

    "return the correct state for example 1" >> {
      val exchange = new Exchange
      exchange.add(Order(Buy, "VOD.L", 1000, 100, ""))
      exchange.add(Order(Sell, "VOD.L", 1000, 100, ""))

      exchange.openOrdersFor("") must beEmpty
      exchange.executedOrdersFor("").toSet must beEqualTo(Set(
        Order(Buy, "VOD.L", 1000, 100, "", Some(100)),
        Order(Sell, "VOD.L", 1000, 100, "", Some(100))
      ))
    }

    "return the correct state for example 2" >> {
      val exchange = new Exchange
      exchange.add(Order(Buy, "VOD.L", 1000, 99, ""))
      exchange.add(Order(Buy, "VOD.L", 1000, 101, ""))
      exchange.add(Order(Sell, "VOD.L", 1000, 100, ""))

      exchange.openOrdersFor("") must beEqualTo(Seq(Order(Buy, "VOD.L", 1000, 99, "")))
      exchange.executedOrdersFor("").toSet must beEqualTo(Set(
        Order(Buy, "VOD.L", 1000, 101, "", Some(100)),
        Order(Sell, "VOD.L", 1000, 100, "", Some(100))
      ))
    }

    "return the correct state for example 3" >> {
      val exchange = new Exchange
      exchange.add(Order(Buy, "VOD.L", 1000, 100, ""))
      exchange.add(Order(Sell, "VOD.L", 1000, 101, ""))

      exchange.openOrdersFor("") must beEqualTo(Seq(
        Order(Buy, "VOD.L", 1000, 100, ""),
        Order(Sell, "VOD.L", 1000, 101, "")))
      exchange.executedOrdersFor("") must beEmpty
    }

    "return the correct state for example 4" >> {
      val exchange = new Exchange
      exchange.add(Order(Buy, "VOD.L", 1000, 100, ""))
      exchange.add(Order(Buy, "VOD.L", 1000, 99, ""))
      exchange.add(Order(Sell, "VOD.L", 1000, 98, ""))

      exchange.openOrdersFor("") must beEqualTo(Seq(
        Order(Buy, "VOD.L", 1000, 99, "")))
      exchange.executedOrdersFor("").toSet must beEqualTo(Set(
        Order(Buy, "VOD.L", 1000, 100, "", Some(98)),
        Order(Sell, "VOD.L", 1000, 98, "", Some(98))
      ))
    }
  }
}
