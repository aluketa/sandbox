package wordwrap

import org.scalatest.{Matchers, FlatSpec}
import wordwrap.WordWrap._

class WordWrapTest extends FlatSpec with Matchers {

  "WordWrap" should "return empty string is no text is specified" in {
    "".wordWrap(10) should be ("")
  }

  it should "work with a single word" in {
    "kata".wordWrap(4) should be ("kata")
  }

  it should "return wrapped text with small sample" in {
    val input = "This kata"
    val expected = "This\nkata"
    input.wordWrap(4) should be (expected)
  }

  it should "return wrapped text" in {
    val input = "This kata should be easy unless there are hidden, or not so hidden, obstacles. Let's start!"
    val expected = "This kata\nshould be\neasy unless\nthere are\nhidden, or\nnot so\nhidden,\nobstacles.\nLet's start!"
    input.wordWrap(12) should be (expected)
  }

}
