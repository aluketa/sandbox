package com.asl.rover

import org.specs2.mutable.Specification

class RoverSystemSpec extends Specification {
  "The Mars Rover System" should {
    val roverSystem = new RoverSystem

    "return the correct output for a given set of test commands" >> {
      val result = roverSystem.processCommands(
        "5 5\n" +
        "0 0 N\n" +
        "MRMLM")

      result must beEqualTo("1 2 N")
    }

    "return the correct output for multiple instructions" >> {
      val result = roverSystem.processCommands(
        "5 5\n" +
        "1 2 N\n" +
        "LMLMLMLMM\n" +
        "3 3 E\n" +
        "MMRMMRMRRM")

      result must beEqualTo(
        "1 3 N\n" +
        "5 1 E")
    }
  }
}
