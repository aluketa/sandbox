package com.asl.encryption

import java.io._

import org.specs2.mutable.SpecificationWithJUnit

class EncryptionSpec extends SpecificationWithJUnit {
  "The Encryption mixin" should {
    "correctly encrypt and decrypt data" >> {
      val byteStream = new ByteArrayOutputStream()
      val outputStream = new PrintStream(byteStream) with Encryption
      outputStream.print("Testing 123")

      val inputStream = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(byteStream.toByteArray) with Decryption))
      val decrypted = inputStream.readLine()
      decrypted must beEqualTo("Testing 123")
    }
  }
}
