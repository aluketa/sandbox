package com.asl.sandbox.twentyone

import org.specs2.mutable.SpecificationWithJUnit

class DeckSpec extends SpecificationWithJUnit {
  isolated

  "The Deck" should {
    val deck: Deck = new SingleDeck

    "return a full deck of playing cards" in {
      val allCards: Set[Card] = (1 to 104).map(n => deck.deal).toSet
      allCards must haveSize(52)

      (2 to 10) foreach { n =>
        allCards.filter(_.name == n.toString) must haveSize(4)
        allCards.filter(_.name == n.toString) foreach { c => c.values must beEqualTo((n, n)) }
      }

      Seq("King", "Queen", "Jack") foreach { n =>
        allCards.filter(_.name == n) foreach { c => c.values must beEqualTo((10, 10)) }
      }

      allCards.filter(_.name == "Ace") foreach { c => c.values must beEqualTo((1, 11)) }

      allCards.filter(_.suit == Suit.Club) must haveSize(52/4)
      allCards.filter(_.suit == Suit.Spade) must haveSize(52/4)
      allCards.filter(_.suit == Suit.Heart) must haveSize(52/4)
      allCards.filter(_.suit == Suit.Diamond) must haveSize(52/4)
      allCards.filter(_.name == "Ace") must haveSize(4)
      allCards.filter(_.name == "King") must haveSize(4)
      allCards.filter(_.name == "Queen") must haveSize(4)
      allCards.filter(_.name == "Jack") must haveSize(4)
    }

    "shuffle the deck between cycles" in {
      val deck1: List[Card] = (1 to 52).map(n => deck.deal).toList
      val deck2: List[Card] = (1 to 52).map(n => deck.deal).toList

      deck1 must haveSize(52)
      deck2 must haveSize(52)
      deck1 mustNotEqual deck2
    }
  }
}
