package com.asl.sandbox.twentyone

import org.specs2.mutable.SpecificationWithJUnit

class ScoreCalculatorSpec extends SpecificationWithJUnit {
  "The score calculator" should {
    "calculate the correct scores for a given set of cards" in {
      val cards = Seq(
        Card("", Suit.Spade, (1, 2)),
        Card("", Suit.Spade, (3, 4)),
        Card("", Suit.Spade, (5, 6)))

      ScoreCalculator.calculateScores(cards) must beEqualTo(Seq(9, 10, 11, 12))
    }

    "calculate the correct scores for a set containing a single card" in {
      ScoreCalculator.calculateScores(Seq(Card("", Suit.Spade, (5, 7)))) must beEqualTo(Seq(5, 7))
    }

    "calculate the correct scores for an empty set of cards" in {
      ScoreCalculator.calculateScores(Seq()) must beEqualTo(Seq())
    }
  }
}
