package com.asl.enigma.en1765

import org.specs2.mutable.SpecificationWithJUnit

class AnswerDeterminerSpec extends SpecificationWithJUnit {
  "The Answer Determiner" should {
    "return the correct boolean result if a given argument is a correct answer" >> {
      AnswerDeterminer.isAnswer(987654321) must beFalse
      AnswerDeterminer.isAnswer(123456789) must beTrue
      AnswerDeterminer.isAnswer(123456799) must beFalse
      AnswerDeterminer.isAnswer(123456989) must beFalse
      AnswerDeterminer.isAnswer(258) must beTrue
      AnswerDeterminer.isAnswer(631) must beFalse
    }
  }
}
